" Vimrc config file by M. A. Hamad (M.m-d)

" General
set encoding=utf-8 
set number	
set relativenumber
set textwidth=110
set showmatch
set undolevels=1000
set ttyfast
set splitbelow 
set splitright


" Start vim in insert mode.
autocmd BufNewFile * startinsert


" Arabic support 
let mapleader = "\<Space>"
" Switch to Arabic - mapping
nnoremap <Leader>a :<C-U>call ShowArabic()<CR>

" Arabic workaround - function
function! ShowArabic()
    set arabicshape
    set noarabicshape
    set rightleft
    set norightleft
endfunction


" Searching
set hlsearch	
set smartcase	
set ignorecase
set incsearch

" Spacing
set autoindent	
set shiftwidth=4
set smartindent	
set smarttab
set softtabstop=4
set expandtab

" Colorscheme
" use this after installing gruvbox:
" cp ~/.vim/plugged/gruvbox/colors/gruvbox.vim ~/.vim/colors/
syntax on
set background=dark
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
let &t_ut=''
set colorcolumn=110
highlight ColorColumn ctermbg=darkgrey
set conceallevel=2

" Status line
set laststatus=2
set wildmenu 
set ruler

" vim-plug: A minimalist Vim plugin manager. run command [ :PlugInstall ]
" Download it using:
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged') 
Plug 'cormacrelf/vim-colors-github'
Plug 'frazrepo/vim-rainbow'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'ycm-core/YouCompleteMe'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons' "Use Fira code font with this plugin
Plug 'yggdroot/indentline'
call plug#end()

""" installing YCM:
" cd .vim/plugged/YouCompleteMe/
" sudo dnf install python3-devel
" python install.py
" python install.py --clangd-completer

" Plugins
map <Leader>f :NERDTreeToggle<CR>
map <Leader>t :TagbarToggle<CR>
let g:rainbow_active = 1
set runtimepath+=~/.vim/bundle/auto-pairs
let g:indentLine_char = '¦'
let g:indentLine_concealcursor = 'inc'
let g:indentLine_color_term = 239
let g:indentLine_conceallevel = 2
set rtp+=~/.vim/bundle/YouCompleteMe

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*


" Auto compile
" set exrc
" set secure
augroup compileAndRun
  autocmd!
  autocmd FileType c map <F5> :w <CR> :!clear; gcc % -o %< && ./%< <CR>
  autocmd FileType cpp map <F5> :w <CR> :!clear; g++ % -o %< && ./%< <CR>
  autocmd FileType cpp map <Leader>c :w <CR> :!clear; g++ -c *.cpp && g++ *.o -o %< && ./%< <CR>
  autocmd FileType python map <F5> :w <CR> :!clear; python % <CR>
  autocmd FileType sh map <F5> :w <CR> :!clear; chmod +x % && ./% <CR>
augroup END


" Debugger
let g:termdebug_popup = 0
let g:termdebug_wide = 80

augroup Debugger
  autocmd!
  autocmd FileType c map <Leader>d :w <CR> :!clear; gcc -g % -o %< <CR> :<C-U>call DebuggerFunc() <CR>
  autocmd FileType cpp map <Leader>d :w <CR> :!clear; g++ -g % -o %< <CR> :<C-U>call DebuggerFunc() <CR>
  autocmd FileType cpp map <Leader>g :w <CR> :!clear; g++ -c -g *.cpp && g++ -g *.o -o %< <CR> :<C-U>call DebuggerFunc() <CR>
augroup END
function! DebuggerFunc()
if (&ft=="c" || &ft=="cpp")
  packadd termdebug
  :Termdebug ./%<
endif
endfunction