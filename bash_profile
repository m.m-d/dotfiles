# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:/var/lib/flatpak/exports/bin:/var/lib/flatpak/exports/share/applications" ]]
then
    PATH="$PATH:$HOME/.local/bin:$HOME/bin:/var/lib/flatpak/exports/bin:/var/lib/flatpak/exports/share/applications"
fi
export PATH

# Setting screen gamma and brightness using redshift
manualRedshift()
{
    if [[ "$(date +%p)" -eq "PM" ]] && [[ "$(date +%I)" -gt 5 ]]
    then
        redshift -b 0.9 -m wayland -PO 4500 &
    elif [[ "$(date +%p)" -eq "AM" ]] && [[ "$(date +%I)" -lt 7 ]]
    then
        redshift -b 0.9 -m wayland -PO 4500 & 
    else
        redshift -b 1 -m wayland -PO 6500 &
    fi
}

startSway()
{
    XDG_SESSION_TYPE=wayland 
    sway &
    # Setting redshift manually for sway
    sleep 10
    manualRedshift
}

# Choices for GUI
if [[ $XDG_SESSION_TYPE -eq "tty" ]] 
then
    printf "\nStart Sway: s \nStart Gnome-shell: g\
	\nStart Xorg: x\n$ "
    read ANSWER
    case $ANSWER in
	([Ss]) startSway ;;
	([Gg]) XDG_SESSION_TYPE=wayland \
	    dbus-run-session -- gnome-shell ;;
	([Xx]) startx gnome-session -- :1 ;;
	(*) :;;
    esac
fi
#